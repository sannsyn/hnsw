package com.tellusr.hnsw;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class HnswTest {

    @Test
    void f32() {
        Hnsw<F32> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(500000)
                .withMetric(Metric.L1)
                .buildF32();
        Util.<Long, Vec<F32>>mapOf(
                0L, Vec.f32(1, 0, 0),
                1L, Vec.f32(0, 1, 1),
                2L, Vec.f32(0, 0, 1),
                3L, Vec.f32(1, 0, 0, 1),
                4L, Vec.f32(1, 1, 1),
                5L, Vec.f32(1, -1, 0)).forEach(hnsw::insert);
        List<Neighbour> res = hnsw.search(Vec.f32(0, 0, 1), 4, 30);
        Assertions.assertEquals(2L, res.get(0).id());
    }

    @Test
    void u16() {
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();
        Util.<Long, Vec<U16>>mapOf(
                0L, Vec.u16("abcd"),
                1L, Vec.u16("efgh"),
                2L, Vec.u16("ijkl"),
                3L, Vec.u16("abc"),
                4L, Vec.u16("test"),
                5L, Vec.u16("abbdc")).forEach(hnsw::insert);
        List<Neighbour>  res = hnsw.search(Vec.u16("abcd"), 4, 30);
        Assertions.assertEquals(0L, res.get(0).id());
    }

    @Test
    void u16_destructor() {
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();

        Util.<Long, Vec<U16>>mapOf(
                0L, Vec.u16("abcd"),
                1L, Vec.u16("efgh"),
                2L, Vec.u16("ijkl"),
                3L, Vec.u16("abc"),
                4L, Vec.u16("test"),
                5L, Vec.u16("abbdc")).forEach(hnsw::insert);
        hnsw.search(Vec.u16("abcd"), 4, 30);


        // And destruct:
        hnsw.drop();
        Assertions.assertThrows(IllegalStateException.class, () -> hnsw.insert(10L, Vec.u16("hello")));
        Assertions.assertThrows(IllegalStateException.class, () -> hnsw.search(Vec.u16("hello"), 10, 10));
    }

    @Test
    void dropRightAfterInit() {
        Assertions.assertDoesNotThrow(Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16()::drop);
    }

    @Test
    void mayOnlyDropOnce() {
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();
        hnsw.drop(); // Drop once.
        Assertions.assertThrows(IllegalStateException.class, hnsw::drop);
    }

    @Test
    void f32_destructor() {
        Hnsw<F32> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.L1)
                .buildF32();

        Util.<Long, Vec<F32>>mapOf(
                0L, Vec.f32(1, 0, 0),
                1L, Vec.f32(0, 1, 1),
                2L, Vec.f32(0, 0, 1),
                3L, Vec.f32(1, 0, 0, 1),
                4L, Vec.f32(1, 1, 1),
                5L, Vec.f32(1, -1, 0)).forEach(hnsw::insert);
        hnsw.search(Vec.f32(1, -1, 0), 4, 30);


        // And destruct:
        hnsw.drop();
        Assertions.assertThrows(IllegalStateException.class, () -> hnsw.insert(10L, Vec.f32(1, -1, 0)));
        Assertions.assertThrows(IllegalStateException.class, () -> hnsw.search(Vec.f32(1, -1, 0), 10, 10));

    }

    @Test
    void u16_illegalSearchParams() {
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();
        Util.<Long, Vec<U16>>mapOf(
                0L, Vec.u16("abcd"),
                1L, Vec.u16("efgh"),
                2L, Vec.u16("ijkl"),
                3L, Vec.u16("abc"),
                4L, Vec.u16("test"),
                5L, Vec.u16("abbdc")).forEach(hnsw::insert);
        Assertions.assertThrows(IllegalArgumentException.class, () -> hnsw.search(Vec.u16("abcd"), -4, 30));
        Assertions.assertThrows(IllegalArgumentException.class, () -> hnsw.search(Vec.u16("abcd"), 4, -30));
    }

}