package com.tellusr.hnsw;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HnswBuilderTest {

    @SuppressWarnings("WriteOnlyObject") // Believe it or not, compiler,
    // I really DO want to check that writing illegally throws! =D
    @Test
    void illegalSettings() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new HnswBuilder().withEfConst(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new HnswBuilder().withMaxNeighborhoodConnections(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new HnswBuilder().withMaxElements(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new HnswBuilder().withMaxLayers(-1));
    }

}