package examples;

import com.tellusr.hnsw.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A simple performance test for doing a search after inserting a million words.
 * <p>
 * The test sets up a data set of 1,000,000 vectors and performs a search on that dataset using {@link Hnsw}.
 * Example output:
 * <pre>
 *     Generating the dataset took: 79 ms
 *
 *  setting number of points 50000
 *  setting number of points 100000
 *  setting number of points 150000
 *  setting number of points 200000
 *  setting number of points 250000
 *  setting number of points 300000
 *  setting number of points 350000
 *  setting number of points 400000
 *  setting number of points 450000
 *  setting number of points 500000
 *  setting number of points 550000
 *  setting number of points 600000
 *  setting number of points 650000
 *  setting number of points 700000
 *  setting number of points 750000
 *  setting number of points 800000
 *  setting number of points 850000
 *  setting number of points 900000
 *  setting number of points 950000
 *  setting number of points 1000000
 * Building the model took: 134 seconds
 *
 * The search took: 8 ms
 *
 * Here are the closest vectors to [0.5, 0.5, 0.5]:
 * vector: [0.5, 0.5, 0.5]
 * vector: [0.5, 0.51, 0.5]
 * vector: [0.51, 0.5, 0.5]
 * vector: [0.5, 0.5, 0.51]
 *
 * Process finished with exit code 0
 * </pre>
 */
class PerformanceTestF32 {
    public static void main(String[] args) {
        Hnsw<F32> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMaxElements(1_000_000)
                .withMaxLayers(18)
                .withMetric(Metric.L1)
                .buildF32();

        // Make a data set of a million vectors sampled from k³ where k=f32.
        // Each vector is contained in the unit cube I³, and the vectors are evenly spread.
        Map<Long, float[]> idToVecAsFloatArray = new HashMap<>();
        {
            long t0 = System.currentTimeMillis();
            long id = 0;
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {
                    for (int k = 0; k < 100; k++) {
                        idToVecAsFloatArray.put(id++, new float[]{i / 100f, j / 100f, k / 100f});
                    }
                }
            }
            System.out.printf("Generating the dataset took: %s ms%n", (System.currentTimeMillis() - t0));
            System.out.println();
        }

        // Register data to the hnsw.
        {
            long t0 = System.currentTimeMillis();
            idToVecAsFloatArray.forEach((id, arr) -> hnsw.insert(id, Vec.f32(idToVecAsFloatArray.get(id))));
            System.out.printf("Building the model took: %s seconds%n", (System.currentTimeMillis() - t0) / 1000L);
            System.out.println();
        }

        // Perform a search:
        final List<Neighbour> res;
        {
            long t0 = System.currentTimeMillis();
            res = hnsw.search(Vec.f32(0.5f, 0.5f, 0.5f), 4, 30);
            System.out.printf("The search took: %s ms%n", (System.currentTimeMillis() - t0));
            System.out.println();
        }

        // Print closest points to the center of I³.
        System.out.println("Here are the closest vectors to [0.5, 0.5, 0.5]:");
        String out = res.stream()
                .map(Neighbour::id)
                .map(idToVecAsFloatArray::get)
                .map(Arrays::toString)
                .map(s -> String.format("vector: %s", s))
                .collect(Collectors.joining("\n"));

        System.out.println(out);
    }
}
