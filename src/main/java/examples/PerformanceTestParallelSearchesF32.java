package examples;

import com.tellusr.hnsw.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Perform many searches in parallell. The program keeps performing searches until it is interrupted,
 * and displays the result of every 10000th search.
 * <p>
 * Example output:
 * <pre>
 *     Generating the dataset took: 76 ms
 *
 *  setting number of points 50000
 *  setting number of points 100000
 *  setting number of points 150000
 *  setting number of points 200000
 *  setting number of points 250000
 *  setting number of points 300000
 *  setting number of points 350000
 *  setting number of points 400000
 *  setting number of points 450000
 *  setting number of points 500000
 *  setting number of points 550000
 *  setting number of points 600000
 *  setting number of points 650000
 *  setting number of points 700000
 *  setting number of points 750000
 *  setting number of points 800000
 *  setting number of points 850000
 *  setting number of points 900000
 *  setting number of points 950000
 *  setting number of points 1000000
 * Building the model took: 131 seconds
 *
 *
 * Will now perform many searches. Will only stop when this program is interrupted.
 *
 * Will now print the result for search number: 10000
 * The search took: 342244 ns
 *
 * Here are the closest vectors to [0.9113088, 0.9403293, 0.30008015]:
 * vector: [0.91, 0.94, 0.3]
 * vector: [0.92, 0.94, 0.3]
 * vector: [0.91, 0.95, 0.3]
 * vector: [0.9, 0.94, 0.3]
 * </pre>
 */
public class PerformanceTestParallelSearchesF32 {
    public static void main(String[] args) {
        Hnsw<F32> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMaxElements(1_000_000)
                .withMaxLayers(18)
                .withMetric(Metric.L1)
                .buildF32();

        // Make a data set of a million vectors sampled from k³ where k=f32.
        // Each vector is contained in the unit cube I³, and the vectors are evenly spread.
        Map<Long, float[]> idToVecAsFloatArray = new HashMap<>();
        {
            long t0 = System.currentTimeMillis();
            long id = 0;
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {
                    for (int k = 0; k < 100; k++) {
                        idToVecAsFloatArray.put(id++, new float[]{i / 100f, j / 100f, k / 100f});
                    }
                }
            }
            System.out.printf("Generating the dataset took: %s ms%n", (System.currentTimeMillis() - t0));
            System.out.println();
        }

        // Register data to the hnsw.
        {
            long t0 = System.currentTimeMillis();
            idToVecAsFloatArray.forEach((id, arr) -> hnsw.insert(id, Vec.f32(idToVecAsFloatArray.get(id))));
            System.out.printf("Building the model took: %s seconds%n", (System.currentTimeMillis() - t0) / 1000L);
            System.out.println();
        }


        System.out.println();
        System.out.println("Will now perform many searches. Will only stop when this program is interrupted.");
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(5);
        AtomicLong counter = new AtomicLong();
        exec.scheduleWithFixedDelay(() -> srch(hnsw, idToVecAsFloatArray::get, counter.incrementAndGet()), 0L, 1L, TimeUnit.MILLISECONDS);
        exec.scheduleWithFixedDelay(() -> srch(hnsw, idToVecAsFloatArray::get, counter.incrementAndGet()), 0L, 1L, TimeUnit.MILLISECONDS);
        exec.scheduleWithFixedDelay(() -> srch(hnsw, idToVecAsFloatArray::get, counter.incrementAndGet()), 0L, 1L, TimeUnit.MILLISECONDS);
        exec.scheduleWithFixedDelay(() -> srch(hnsw, idToVecAsFloatArray::get, counter.incrementAndGet()), 0L, 1L, TimeUnit.MILLISECONDS);
    }

    /**
     * @param hnsw                the hnsw.
     * @param idToVecAsFloatArray a map from ID to float array.
     * @param iteration           which iteration? Used to print periodic messages.
     */
    private static void srch(Hnsw<F32> hnsw,
                             Function<Long, float[]> idToVecAsFloatArray,
                             long iteration) {
        float a = randomFlt(), b = randomFlt(), c = randomFlt();
        long t0 = System.nanoTime();
        List<Neighbour> r = search(hnsw, a, b, c);
        if (iteration % 10000L == 0) {
            long time = (System.nanoTime() - t0);
            System.out.println();
            System.out.println("Will now print the result for search number: " + iteration);
            System.out.printf("The search took: %s ns%n", time);
            System.out.println();
            System.out.printf("Here are the closest vectors to [%s, %s, %s]:%n", a, b, c);
            String out = r.stream()
                    .map(Neighbour::id)
                    .map(idToVecAsFloatArray)
                    .map(Arrays::toString)
                    .map(s -> String.format("vector: %s", s))
                    .collect(Collectors.joining("\n"));
            System.out.println(out);
        }
    }

    private static List<Neighbour> search(Hnsw<F32> hnsw, float a, float b, float c) {
        return hnsw.search(Vec.f32(a, b, c), 4, 30);
    }

    private static float randomFlt() {
        return (float) (Math.random() % 1);
    }

}
