package examples;

import com.tellusr.hnsw.F32;
import com.tellusr.hnsw.Hnsw;
import com.tellusr.hnsw.Metric;
import com.tellusr.hnsw.Vec;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// A test program to benchmark native memory consumption and clearing.
// How to reproduce test:
// 1) Run this program while adding -Xmx2g as VM option.
// 2) Run top and watch memory; if this is the only program that is running, then the memory consumption should stagnate.
// NB: If 1) is skipped then the JVM will keep allocating memory until it reaches a certain fraction of the total machine memory.
//     This allocation is slow, so then it may look like the memory consumption does not stagnate,
//     even though the native memory consumption does stagnate.
//     The total memory consumed by this program is M = mem(JVM) + mem(rust).
//     Assuming mem(JVM) = 2g, then we want to see that M is constant.
public class DeallocateNativeMemory {
    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) {
        long count = 0;
        for (; ; ) {
            Hnsw<F32> hnsw = Hnsw.builder()
                    .withMaxLayers(20)
                    .withMaxElements(100000)
                    .withMaxNeighborhoodConnections(15)
                    .withEfConst(500000)
                    .withMetric(Metric.L1)
                    .buildF32();
            DeallocateNativeMemory.<Long,Vec<F32>>mapOf(
                    0L, Vec.f32(1, 0, 0),
                    1L, Vec.f32(0, 1, 1),
                    2L, Vec.f32(0, 0, 1),
                    3L, Vec.f32(1, 0, 0, 1),
                    4L, Vec.f32(1, 1, 1),
                    5L, Vec.f32(1, -1, 0)).forEach(hnsw::insert);
            hnsw.drop();
            if (count % 1_000_000 == 0)
                System.out.println(count);
            count++;
        }
    }

    /**
     * Helper method copied from com.tellusr.hnsw.Util to avoid making Util public.
     */
    @SuppressWarnings("DuplicatedCode")
    static <K,V> Map<K,V> mapOf(Object... args) {
        if (args.length == 0) return Collections.emptyMap();
        if (args.length % 2 != 0) throw new IllegalArgumentException();
        Map<K,V> m = new HashMap<>();
        for (int i = 0; i < args.length/2; i = i + 2) {
            m.put((K) args[i], (V) args[i+1]);
        }
        return m;
    }
}
