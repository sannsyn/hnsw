package examples;

import com.sun.jna.Pointer;
import com.tellusr.hnsw.PointerFactory;
import com.tellusr.hnsw.U16;
import com.tellusr.hnsw.Vec;
import com.tellusr.hnsw.internal.HnswRs;
import com.tellusr.hnsw.internal.Neighbour_api;
import com.tellusr.hnsw.internal.Neighbourhood_api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains an example program for {@link HnswRs} - the internal hnsw-rs link.
 */
class Example_u16 {

    private static long unsigned(long l) {
        return l;
    }

    public static void main(String[] args) {
        long max_nb_connection = 15;
        String dist = "DistLevenshtein";
        long ef_c = 200;
        Pointer hnsw_u16 = HnswRs.lib().init_hnsw_u16(max_nb_connection,
                ef_c,
                dist.length(),
                PointerFactory.utf8(dist));
        long i = 0;
        Map<Long, String> wordByIndex = new HashMap<>();
        for (String w : Arrays.asList("abcd", "efgh", "ijkl", "abc", "test", "abbdc")) {
            Vec<U16> ref = Vec.u16(w.toCharArray());
            HnswRs.lib().insert_u16(hnsw_u16, ref.size(), ref.pointer(), i);
            wordByIndex.put(i, w);
            i++;
        }
        String searchWord = "abcd";
        Vec<U16> searchWordVector = Vec.u16(searchWord.toCharArray());
        long ef_search = 30;
        Neighbourhood_api res = HnswRs.lib().search_neighbours_u16(hnsw_u16,
                searchWordVector.size(),
                searchWordVector.pointer(),
                wordByIndex.size(),
                ef_search);
        System.out.println("Target word: " + searchWord);
        System.out.println("Number of closest neighbors: " + res.nbgh);
        res.neighbours().forEach(n -> System.out.printf("Word: %s distance: %s%n", wordByIndex.get(n.id), n.d));
    }

}
