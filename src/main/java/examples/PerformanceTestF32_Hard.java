package examples;

import com.tellusr.hnsw.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A simple performance test for doing a search after inserting a million words.
 * <p>
 * The test sets up a data set of 1,000,000 vectors and performs a search on that dataset using {@link Hnsw}.
 * Example output:
 * <pre>
 *     Generating the dataset took: 79 ms
 *
 *  setting number of points 50000
 *  setting number of points 100000
 *  setting number of points 150000
 *  setting number of points 200000
 *  setting number of points 250000
 *  setting number of points 300000
 *  setting number of points 350000
 *  setting number of points 400000
 *  setting number of points 450000
 *  setting number of points 500000
 *  setting number of points 550000
 *  setting number of points 600000
 *  setting number of points 650000
 *  setting number of points 700000
 *  setting number of points 750000
 *  setting number of points 800000
 *  setting number of points 850000
 *  setting number of points 900000
 *  setting number of points 950000
 *  setting number of points 1000000
 * Building the model took: 134 seconds
 *
 * The search took: 8 ms
 *
 * Here are the closest vectors to [0.5, 0.5, 0.5]:
 * vector: [0.5, 0.5, 0.5]
 * vector: [0.5, 0.51, 0.5]
 * vector: [0.51, 0.5, 0.5]
 * vector: [0.5, 0.5, 0.51]
 *
 * Process finished with exit code 0
 * </pre>
 */
class PerformanceTestF32_Hard {
    public static void main(String[] args) {
        var maxElements = 5_000_000;
        var hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMaxElements(maxElements)
                .withMaxLayers(18)
                .withMetric(Metric.L2)
                .buildF32();

        // Make a data set of a million vectors sampled from k³ where k=f32.
        // Each vector is contained in the unit cube I³, and the vectors are evenly spread.
        var idToVecAsFloatArray = new HashMap<Long, float[]>();
        var r = new Random(0);
        {
            var t0 = System.currentTimeMillis();
            var id = 0L;
            for (int i = 0; i < maxElements; i++) {
                float[] vec = generateRandomVector(r);
                idToVecAsFloatArray.put(id++, vec);
            }
            System.out.printf("Generating the dataset took: %s ms%n", (System.currentTimeMillis() - t0));
            System.out.println();
        }

        // Register data to the hnsw.
        {
            var t0 = System.currentTimeMillis();
            idToVecAsFloatArray.entrySet().parallelStream().forEach(e -> hnsw.insert(e.getKey(), Vec.f32(e.getValue())));
            System.out.printf("Building the model took: %s seconds%n", (System.currentTimeMillis() - t0) / 1000L);
            System.out.println();
        }

        IntStream.range(0, 300).forEach(dontUse -> {
            System.out.println("Generating random query vector.");
            var v = generateRandomVector(r);
            // Perform a search:
            final List<Neighbour> res;
            {
                long t0 = System.currentTimeMillis();
                res = hnsw.search(Vec.f32(v), 50_000, 512);
                System.out.printf("The search took: %s ms%n", (System.currentTimeMillis() - t0));
            }
            System.out.printf("The result had: %s vectors.%n", res.size());
        });
    }

    private static float[] generateRandomVector(Random r) {
        var dim = 300;
        var vec = new float[dim];
        for (var coord = 0; coord < dim; coord++) {
            vec[coord] = r.nextFloat();
        }
        return vec;
    }
}
