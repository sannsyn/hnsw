package examples;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.tellusr.hnsw.F32;
import com.tellusr.hnsw.Vec;

class Print_f32 {

    public static void main(String[] args) {
        RustFunctions.INSTANCE.print_f32(10);
        Vec<F32> v = Vec.f32(1, 2, -1, 0);
        RustFunctions.INSTANCE.print_f32_pointer(new float[]{1, 2, -1, 0}.length, v.pointer());
    }

    interface RustFunctions extends Library {
        RustFunctions INSTANCE = Native.load("rust_functions", RustFunctions.class);

        void print_f32(float l);

        void print_f32_pointer(long len, Pointer l);
    }

}
