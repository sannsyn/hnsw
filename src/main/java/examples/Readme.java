package examples;

import com.tellusr.hnsw.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Readme {
    public static void main(String[] args) {
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMaxElements(20000)
                .withMaxLayers(18)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();

        hnsw.insert(0L, Vec.u16("abcd"));
        hnsw.insert(1L, Vec.u16("efgh"));
        hnsw.insert(2L, Vec.u16("ijkl"));
        hnsw.insert(3L, Vec.u16("abc"));
        hnsw.insert(4L, Vec.u16("test"));
        hnsw.insert(5L, Vec.u16("abbdc"));

        List<Neighbour> res = hnsw.search(Vec.u16("abcd"), 4, 30);
        System.out.println(res);
        hnsw.drop();
    }
}
