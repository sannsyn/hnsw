package examples;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.tellusr.hnsw.U16;
import com.tellusr.hnsw.Vec;

class Print_u16 {

    public static void main(String[] args) {
        RustFunctions.INSTANCE.print_u16(10);
        Vec<U16> v = Vec.u16(1, 2, 100, 44, 556);
        RustFunctions.INSTANCE.print_u16_pointer(v.size(), v.pointer());
    }

    interface RustFunctions extends Library {
        RustFunctions INSTANCE = Native.load("rust_functions", RustFunctions.class);

        void print_u16(long l);

        void print_u16_pointer(long len, Pointer l);
    }

}
