package examples;

import com.sun.jna.Pointer;
import com.tellusr.hnsw.F32;
import com.tellusr.hnsw.PointerFactory;
import com.tellusr.hnsw.Vec;
import com.tellusr.hnsw.internal.HnswRs;
import com.tellusr.hnsw.internal.Neighbourhood_api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains an example program for {@link HnswRs} - the internal hnsw-rs link.
 */
class Example_f32 {

    public static void main(String[] args) {
        HnswRs.lib().init_rust_log(); // Doesn't work?
        long nb_elem = 500000; // Number of possible words in the dictionary.
        long max_nb_connection = 15;
        String dist = "DistL1";
        Pointer hnsw_f32 = HnswRs.lib().init_hnsw_f32(max_nb_connection, nb_elem, dist.length(), PointerFactory.utf8(dist));
        long i = 0;
        Map<Long, float[]> wordByIndex = new HashMap<>();
        for (float[] w : Arrays.asList(
                new float[]{1, 0, 0},
                new float[]{0, 1, 1},

                new float[]{0, 0, 1}, // Search for this.

                new float[]{1, 0, 0, 1},
                new float[]{1, 1, 1},
                new float[]{1, -1, 0})) {
            Vec<F32> ref = Vec.f32(w);
            wordByIndex.put(i, w);
            HnswRs.lib().insert_f32(hnsw_f32, ref.size(), ref.pointer(), i);
            i++;
        }
        float[] searchWord = new float[]{0, 0, 1};
        Vec<F32> searchWordRef = Vec.f32(searchWord);
        Neighbourhood_api res = HnswRs.lib().search_neighbours_f32(hnsw_f32,
                searchWordRef.size(),
                searchWordRef.pointer(),
                4,
                30);
        System.out.println("Target word: " + Arrays.toString(searchWord));
        System.out.println("Number of closest neighbors: " + res.nbgh);
        res.neighbours().forEach(n -> System.out.printf("Word: %s distance: %s%n", Arrays.toString(wordByIndex.get(n.id)), n.d));
    }

}
