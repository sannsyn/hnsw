package com.tellusr.hnsw;

import com.sun.jna.Pointer;

/**
 * An interface for insertions into a native hnsw. Can pass as either:
 * <ul>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#insert_f32(Pointer, long, Pointer, long)}</li>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#insert_u16(Pointer, long, Pointer, long)}</li>
 * </ul>
 */
interface Inserter {
    void insert(Pointer hnsw, long len, Pointer data, long id);
}
