package com.tellusr.hnsw;

import com.sun.jna.Pointer;
import com.tellusr.hnsw.internal.Neighbourhood_api;

/**
 * An interface for searches on a native hnsw instance. Can pass as either:
 * <ul>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#search_neighbours_f32(Pointer, long, Pointer, long, long)}</li>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#search_neighbours_u16(Pointer, long, Pointer, long, long)}</li>
 * </ul>
 */
interface Searcher {
    Neighbourhood_api searchNeighbours(Pointer hnsw, long len, Pointer data, long knbn, long ef_search);
}
