package com.tellusr.hnsw;

/**
 * As in 'vector space over a field'. This field could be u16 or f32. This interface is used
 * to implement typechecking in {@link Hnsw}.
 */
// With java 17+, make this a sealed interface.
public interface Field {
}
