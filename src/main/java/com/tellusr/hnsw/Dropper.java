package com.tellusr.hnsw;

import com.sun.jna.Pointer;

/**
 * An interface for clearing allocated memory for hnsw instances. Can pass as either:
 * <ul>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#drop_hnsw_f32(Pointer)}</li>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#drop_hnsw_u16(Pointer)}</li>
 * </ul>
 */
public interface Dropper {
    void drop(Pointer p);
}
