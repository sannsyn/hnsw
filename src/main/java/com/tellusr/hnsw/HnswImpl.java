package com.tellusr.hnsw;

import com.sun.jna.Pointer;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

// A canonical implementation of Hnsw.
final class HnswImpl<T extends Field> implements Hnsw<T> {

    // Fields
    private final Pointer hnsw;
    private final Metric metric;
    private final long maxNeighborhoodConnections;
    private final long efConst;
    private final long maxElements;
    private final long maxLayer;
    private final Searcher searcher;
    private final Inserter inserter;
    private final Dropper dropper;

    // State
    private boolean isDropped = false; // To be dropped means that any allocated native memory has been cleared.

    HnswImpl(Pointer hnsw,
             Metric metric,
             long maxNeighborhoodConnections,
             long efConst,
             long maxElements, long maxLayer, Searcher searcher, Inserter insert, Dropper dropper) {
        this.hnsw = hnsw;
        this.metric = metric;
        this.maxNeighborhoodConnections = maxNeighborhoodConnections;
        this.efConst = efConst;
        this.maxElements = maxElements;
        this.maxLayer = maxLayer;
        this.searcher = searcher;
        this.inserter = insert;
        this.dropper = dropper;
    }

    // Accessors

    @Override
    public Metric metric() {
        return metric;
    }

    @Override
    public long maxNeighborhoodConnections() {
        return maxNeighborhoodConnections;
    }

    @Override
    public long efConst() {
        return efConst;
    }

    @Override
    public long maxElements() {
        return maxElements;
    }

    @Override
    public long maxLayer() {
        return maxLayer;
    }

    // Hnsw operations

    @Override
    public void insert(long id, Vec<T> vec) {
        if (isDropped)
            throw new IllegalStateException("This hnsw's native memory has been cleared and can no longer be used for insertions.");
        long size = vec.size();
        Util.requireNonNegative(size);
        Util.requireNonNegative(id);
        inserter.insert(hnsw, size, vec.pointer(), id);
    }

    @Override
    public List<Neighbour> search(Vec<T> target, long kbnb, long efSearch) {
        if (isDropped)
            throw new IllegalStateException("This hnsw's native memory has been cleared and can no longer be used for searches.");
        Util.requireNonNegative(kbnb);
        Util.requireNonNegative(efSearch);
        long tSize = target.size();
        Util.requireNonNegative(tSize);
        return Collections.unmodifiableList(searcher
                .searchNeighbours(hnsw,
                        tSize,
                        target.pointer(),
                        kbnb,
                        efSearch)
                .neighbours()
                .collect(Collectors.toList()));
    }

    // Memory management

    @Override
    public synchronized void drop() {
        if (isDropped) {
            throw new IllegalStateException("This Hnsw instance was already dropped.");
        }
        isDropped = true;
        dropper.drop(hnsw);
    }
}
