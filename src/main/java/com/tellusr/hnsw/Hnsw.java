package com.tellusr.hnsw;

import java.util.List;

/**
 * A java interface for the hnsw implementation found here: <a href="https://github.com/jean-pierreBoth/hnswlib-rs">hnswlib-rs</a>
 * <p>
 * To set up an instance, use the {@link #builder()}. E.g.
 * <pre>{@code
 *
 *         // Set up an hnsw model over u16.
 *         Hnsw<U16> hnsw = Hnsw.builder()
 *                 .withMaxNeighborhoodConnections(15)
 *                 .withEfConst(200)
 *                 .withMaxElements(20000)
 *                 .withMaxLayers(18)
 *                 .withMetric(Metric.LEVENSHTEIN)
 *                 .buildU16();
 *
 *         // Set up u16 vectors.
 *         var idToVec = Map.of(
 *                 0L, Vec.u16("abcd"),
 *                 1L, Vec.u16("efgh"),
 *                 2L, Vec.u16("ijkl"),
 *                 3L, Vec.u16("abc"),
 *                 4L, Vec.u16("test"),
 *                 5L, Vec.u16("abbdc")
 *         );
 *
 *         // Build the model.
 *         idToVec.forEach((Long id, Vec<U16> v) -> hnsw.insert(id, v));
 *
 *         // Perform a search.
 *         var res = hnsw.search(Vec.u16("abcd"), 4, 30);
 *         System.out.println(res);
 *
 *         // To deallocate native memory.
 *         hnsw.drop();
 * }</pre>
 *
 * @implNote A {@link Metric} need not be defined for all possible {@link Field}s. If an illegal combination is
 *           is passed to the native library, this Java library makes no attempt at cathing this.
 *
 * @param <T> over which field should the vector space be?
 * @see Field
 */
public interface Hnsw<T extends Field> {

    /**
     * @return a builder for hnsw.
     */
    static HnswBuilder builder() {
        return new HnswBuilder();
    }

    /**
     * @return which metric to use?
     */
    @SuppressWarnings("unused")
    // This is useful information.
    Metric metric();

    /**
     * @return maximal number of neighborhood connections.
     */
    @SuppressWarnings("unused")
    // This is useful information.
    long maxNeighborhoodConnections();

    /**
     * @return the ef constant.
     */
    @SuppressWarnings("unused")
    // This is useful information.
    long efConst();

    /**
     * @return the maximal number of elements that this hnsw can hold.
     */
    @SuppressWarnings("unused")
    // This is useful information.
    long maxElements();

    /**
     * @return the maximal number of layers.
     */
    @SuppressWarnings("unused")
    // This is useful information.
    long maxLayer();

    /**
     * Register a new vector into the model.
     * <pre>{@code
     * var hnsw = Hnsw.builder()
     *         .withMaxNeighborhoodConnections(15)
     *         .withEfConst(200)
     *         .withMetric(Metric.LEVENSHTEIN)
     *         .buildU16();
     *
     * hnsw.insert(Vec.u16("hello"), 0L);}</pre>
     *
     * @param id  the ID of the vector.
     * @param vec the vector to register.
     */
    void insert(long id, Vec<T> vec);

    /**
     * @param target   the vector to search for.
     * @param kbnb     a search parameter.
     * @param efSearch a search parameter.
     * @return a list of the closest matches.
     */
    List<Neighbour> search(Vec<T> target, long kbnb, long efSearch);

    /**
     * Deallocates native memory used by this instance.
     * This method should be called exactly once right before an hnsw can be garbage collected.
     *
     * @implNote After calling this method, this hnsw instance can no longer perform searches and insertions.
     */
    void drop();

}
