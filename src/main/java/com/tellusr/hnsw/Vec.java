package com.tellusr.hnsw;

import com.sun.jna.Memory;
import com.sun.jna.Native;

/**
 * Models a Rust vector of constant size. In order to pass to a native library, the vector is represented as a pointer to a memory segment,
 * and a size that determines the number of elements that the memory segment consists of.
 * <p>
 * This class does not carry type info at runtime, but instead provides factory methods to create vectors of a certain type
 * from java objects.
 * <pre>{@code
 *      Vec<U16> v1 = Vec.u16("hello");
 *      Vec<F32> v2 = Vec.f32(0f, 0f, 1f);
 * }</pre>
 *
 * @implNote The addition of generics provides compile time type checks when interacting with instances of {@link Hnsw}.
 */
// This class can be a record with java 16+.
@SuppressWarnings("unused")
// The type parameter might be unused in the sense this class does not consumer/return instances of T.
// However, omitting T altogether, makes it so that interacting with Hnsw instances lose compile time checks for
// generics mistakes. E.g. insert Vec<U16> into Hnsw<F32> will not be highlighted as an error.
public final class Vec<T extends Field> {

    // Fields
    private final Memory pointer;
    private final long size;

    // Hidden constructor.
    private Vec(Memory p, long size) {
        this.pointer = p;
        this.size = size;
    }

    /**
     * Generate a vector that holds f32 as data.
     *
     * @param values the data to pass.
     * @return the vector.
     */
    public static Vec<F32> f32(float... values) {
        Memory m = new Memory(((long) values.length) * Native.getNativeSize(Float.TYPE));
        m.write(0, values, 0, values.length);
        return new Vec<>(m, values.length);
    }

    /**
     * @return the pointer to native memory that this vector uses.
     */
    public Memory pointer() {
        return pointer;
    }

    /**
     * @return the number of elements that this vector holds.
     */
    public long size() {
        return size;
    }

    /**
     * Generate a vector that holds u16 as data.
     * Takes in non-negative ints, but only takes half of the bytes that makes up the int in order to produce u16s.
     *
     * @param values the data to pass.
     * @return the vector.
     */
    public static Vec<U16> u16(int... values) {
        long ptrSize = ((long) values.length) * Native.getNativeSize(Short.TYPE); // Short is 16 bit.
        Memory m = new Memory(ptrSize);
        long counter = 0;
        for (int value : values) {
            if (value < 0) throw new IllegalArgumentException("Values must be positive.");
            byte b3 = (byte) (value >>> 8);
            byte b4 = (byte) value;
            m.setByte(counter++, b4);
            m.setByte(counter++, b3);
        }
        return new Vec<>(m, values.length);
    }

    /**
     * Generate a vector that holds u16 as data.
     *
     * @param values the data to pass.
     * @return the vector.
     */
    public static Vec<U16> u16(char... values) {
        long ptrSize = ((long) values.length) * Native.getNativeSize(Short.TYPE); // Short is 16 bit.
        Memory m = new Memory(ptrSize);
        long counter = 0;
        for (char value : values) {
            byte b3 = (byte) (value >>> 8);
            byte b4 = (byte) value;
            m.setByte(counter++, b4);
            m.setByte(counter++, b3);
        }
        return new Vec<>(m, values.length);
    }

    /**
     * Generate a vector that holds u16 as data.
     *
     * @param s the string whose characters determine the vector.
     * @return the vector.
     */
    public static Vec<U16> u16(String s) {
        return Vec.u16(s.toCharArray());
    }

}
