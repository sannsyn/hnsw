package com.tellusr.hnsw.internal;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.tellusr.hnsw.Neighbour;

/**
 * @implNote This should be considered an internal class of this library.
 * The only reason this class is public is that it has to be due to the way JNA works.
 */
@Structure.FieldOrder({"id", "d"})
public class Neighbour_api extends Structure implements Neighbour {
    public Neighbour_api(Pointer pointer) {
        super(pointer);
        read();
    }

    final public long id;
    final public float d;

    {
        id = -1;
        d = -1;
        read(); // Will read the fields from native memory.
    }

    @Override
    public String toString() {
        return "Neighbour{" +
                "id=" + id +
                ", distance=" + d +
                '}';
    }

    @SuppressWarnings("unused")
    @Override
    public float distance() {
        return d;
    }

    @Override
    public long id() {
        return id;
    }
}
