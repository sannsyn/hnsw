package com.tellusr.hnsw.internal;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class models a neighbourhood of search results.
 *
 * @implNote This should be considered an internal class of this library.
 * The only reason this class is public is that it has to be due to the way JNA works.
 */
@Structure.FieldOrder({"nbgh", "neighbours"})
public class Neighbourhood_api extends Structure {
    /**
     * The number of nearest neighbours asked for.
     */
    public final long nbgh;
    /**
     * Points to an array of {@link Neighbour_api} of length {@link #nbgh}.
     */
    public final Pointer neighbours;

    {
        nbgh = 0;
        neighbours = null;
        read(); // Will read class fields from native memory.
        requireNbghNonnegative();
    }

    private void requireNbghNonnegative() {
        if (nbgh < 0) throw new IllegalArgumentException("nbgh should be unsigned, but was negative.");
    }

    @Override
    public String toString() {
        return "Neighbourhood_api{" +
                "nbgh=" + nbgh +
                ", neighbours=" + neighbours().collect(Collectors.toList()) +
                '}';
    }

    private Iterator<Neighbour_api> neighboursIter() {
        return new Iterator<Neighbour_api>() {
            long cursor = 0;
            long bytesTraversed = 0;

            @Override
            public boolean hasNext() {
                return cursor < nbgh;
            }

            @Override
            public Neighbour_api next() {
                Objects.requireNonNull(neighbours);
                Neighbour_api ret = new Neighbour_api(neighbours.share(bytesTraversed));
                cursor++;
                bytesTraversed += ret.size();
                return ret;
            }
        };
    }

    /**
     * @return a finite stream of the neighbours residing in this neighbourhood.
     */
    public Stream<Neighbour_api> neighbours() {
        Iterator<Neighbour_api> iter = neighboursIter();
        return StreamSupport.stream(((Iterable<Neighbour_api>) () -> iter).spliterator(), false);
    }

    @SuppressWarnings("unused")
    public static class ByValue extends Neighbourhood_api implements Structure.ByValue {
    }

    @SuppressWarnings("unused")
    public static class ByReference extends Neighbourhood_api implements Structure.ByReference {
    }

}
