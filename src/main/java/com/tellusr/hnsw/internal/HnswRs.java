package com.tellusr.hnsw.internal;


import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.tellusr.hnsw.Hnsw;

/**
 * This interface is for <a href="https://github.com/jean-pierreBoth/hnswlib-rs/blob/master/src/libext.rs">hnsw api</a>.
 * This interface is used to implement the more Java idiomatic {@link Hnsw}.
 * It is recommended to use the latter instead of this as interface is to be considered part of the internals of this library.
 *
 * @see Hnsw
 */
public interface HnswRs extends Library {

    // The library. Loaded from /resources/linux-x86-64/libhnsw_rs.so.
    /**
     * This instance loads the required native library and implements the java-interface.
     */
    HnswRs INSTANCE = Native.load("hnsw_rs", HnswRs.class);

    /**
     * @return this library.
     */
    @SuppressWarnings("SameReturnValue")
    static HnswRs lib() {
        return INSTANCE;
    }

    // Allegedly fires up the logs, but nothing so far...
    void init_rust_log();

    /**
     * Sets up an hnsw instance for f32 and returns a pointer to it.
     *
     * @param max_nb_conn The maximum number of links from one point to others. Values ranging from 16 to 64 are standard initialising values, the higher the more time consuming.
     * @param ef_const    This parameter controls the width of the search for neighbours during insertion. Values from 200 to 800 are standard initialising values, the higher the more time consuming.
     * @param namelen     The length of the distance metric.
     * @param cdistname   The name of the distance metric to be used as a pointer.
     * @return a pointer to the hnsw instance.
     * @see <a href="https://github.com/jean-pierreBoth/hnswlib-rs">citation</a>
     */
    Pointer init_hnsw_f32(long max_nb_conn, long ef_const, long namelen, Pointer cdistname);

    /**
     * Sets up an hnsw instance for f32 and returns a pointer to it.
     *
     * @param max_nb_conn  The maximum number of links from one point to others. Values ranging from 16 to 64 are standard initialising values, the higher the more time consuming.
     * @param ef_const     This parameter controls the width of the search for neighbours during insertion. Values from 200 to 800 are standard initialising values, the higher the more time consuming.
     * @param namelen      The length of the distance metric.
     * @param cdistname    The name of the distance metric to be used as a pointer.
     * @param max_elements The maximum
     * @param max_layer    The maximal number of layers.
     * @return a pointer to the hnsw instance.
     * @see <a href="https://github.com/jean-pierreBoth/hnswlib-rs">citation</a>
     */
    Pointer new_hnsw_f32(long max_nb_conn, long ef_const, long namelen, Pointer cdistname, long max_elements, long max_layer);

    /**
     * Sets up an hnsw instance for u16 and returns a pointer to it.
     *
     * @see #init_hnsw_f32(long, long, long, Pointer)
     */
    Pointer init_hnsw_u16(long max_nb_conn, long ef_const, long namelen, Pointer cdistname);

    /**
     * Sets up an hnsw instance for f32 and returns a pointer to it.
     *
     * @see #new_hnsw_f32(long, long, long, Pointer, long, long)
     */
    Pointer new_hnsw_u16(long max_nb_conn, long ef_const, long namelen, Pointer cdistname, long max_elements, long max_layer);

    /**
     * Insert the given data into the hnsw f32 instance given by a pointer.
     *
     * @param hnsw a pointer that points to an hnsw instance.
     * @param len  the length of the pointer.
     * @param data data to be inserted. Should be a pointer corresponding to {@code len * bytes(f32)} of information.
     * @param id   the ID of the inserted data.
     */
    void insert_f32(Pointer hnsw, long len, Pointer data, long id);

    /**
     * Insert the given data into the hnsw u16 instance given by a pointer.
     *
     * @see #insert_f32(Pointer, long, Pointer, long)
     */
    void insert_u16(Pointer hnsw, long len, Pointer data, long id);

    /**
     * @param hnsw      a pointer to a hnsw f32 instance in native memory. For example, a pointer returned by {@link #new_hnsw_f32(long, long, long, Pointer, long, long)}
     * @param len       the length of the memory segment mentioned below.
     * @param data      a pointer that points to a memory segment that should represent an array of f32.
     * @param knbn      a parameter that controls how many nearest neighbors are searched.
     * @param ef_search a search parameter.
     * @return a neighborhood of search results.
     */
    Neighbourhood_api search_neighbours_f32(Pointer hnsw, long len, Pointer data, long knbn, long ef_search);

    /**
     * Takes a pointer to a hnsw u16 instance and performs a search on it.
     *
     * @return a neighborhood of search results.
     * @see #search_neighbours_f32(Pointer, long, Pointer, long, long)
     */
    Neighbourhood_api search_neighbours_u16(Pointer hnsw, long len, Pointer data, long knbn, long ef_search);

    /**
     * @param p a pointer to a hnsw instance over f32.
     */
    void drop_hnsw_f32(Pointer p);

    /**
     * @param p a pointer to a hnsw instance over u16.
     */
    void drop_hnsw_u16(Pointer p);
}
