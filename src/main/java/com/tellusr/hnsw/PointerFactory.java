package com.tellusr.hnsw;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import java.nio.charset.StandardCharsets;

/**
 * This is a factory for pointers.
 */
public final class PointerFactory {

    private PointerFactory() {
    }

    /**
     * Creates a pointer to a native memory segment that carries the given string as UTF-8.
     *
     * @param s a string.
     * @return the pointer specified.
     */
    public static Pointer utf8(String s) {
        byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
        long ptrSize = ((long) bytes.length) * Native.getNativeSize(Byte.TYPE);
        Memory m = new Memory(ptrSize);
        m.write(0L, bytes, 0, bytes.length);
        return m;
    }

}
