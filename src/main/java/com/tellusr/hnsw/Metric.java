package com.tellusr.hnsw;

/**
 * A parameter for {@link Hnsw} that determines which distance metric to use in the model.
 * <p>
 * Not all ({@link Metric}, {@link Field}) pairs are well-defined. For example, the {@link #LEVENSHTEIN} metric
 * only makes sense for {@link U16}.
 *
 * @see Hnsw
 */
public enum Metric {
    L1("DistL1"),
    L2("DistL2"),
    DOT("DistDot"),
    HELLINGER("DistHellinger"),
    JEFFREYS("DistJeffreys"),
    JENSEN_SHANNON("DistJensenShannon"),
    LEVENSHTEIN("DistLevenshtein"),
    COSINE("DistCosine");

    private final String id;

    Metric(String id) {
        this.id = id;
    }

    /**
     * @return an ID with the property that it matches the accepted strings by {@link HnswConstructor}.
     */
    String id() {
        return id;
    }
}
