package com.tellusr.hnsw;

/**
 * Models the u16 data type from rust. This class is used to get compile time generics checks for {@link Hnsw}.
 */
public final class U16 implements Field {
    private U16() {
    }
}