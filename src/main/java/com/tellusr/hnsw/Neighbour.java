package com.tellusr.hnsw;

/**
 * Given a point <i>p</i>, a <b>neighbour</b> of <i>p</i> is a point <i>p</i>', given by an ID, together with a distance d(<i>p</i>,<i>p</i>').
 */
public interface Neighbour {
    /**
     * @return the distance of this neighbour relative to some target ID.
     */
    @SuppressWarnings("unused")
    float distance();

    /**
     * @return the ID of this neighbour.
     */
    long id();
}
