package com.tellusr.hnsw;

import com.sun.jna.Pointer;
import com.tellusr.hnsw.internal.HnswRs;

/**
 * A reusable builder for {@link Hnsw}. It is reusable in the sense can you can call the {@link HnswBuilder#buildU16()} and {@link HnswBuilder#buildF32()}
 * methods multiple times to obtain multiple instances of {@link Hnsw}.
 *
 * @see Hnsw
 */
public final class HnswBuilder {

    // Class variables
    private Metric metric = Metric.L2;
    private long maxNeighborhoodConnections;
    private long maxElements = 10_000;
    private long maxLayer = 16;
    private long efConst;

    // Package private.
    HnswBuilder() {
    }

    /**
     * Defines which metric to use.
     *
     * @param metric a metric.
     * @return this builder.
     */
    public HnswBuilder withMetric(Metric metric) {
        this.metric = metric;
        return this;
    }

    /**
     * @param maxNeighborhoodConnections the maximal neighborhood connections.
     * @return this builder.
     */
    public HnswBuilder withMaxNeighborhoodConnections(long maxNeighborhoodConnections) {
        if (maxNeighborhoodConnections < 0)
            throw new IllegalArgumentException("The maximal number of neighborhood connections must be positive.");
        this.maxNeighborhoodConnections = maxNeighborhoodConnections;
        return this;
    }

    /**
     * @param efConst the ef constant.
     * @return this builder.
     */
    public HnswBuilder withEfConst(long efConst) {
        if (efConst < 0) throw new IllegalArgumentException("The ef constant must be positive.");
        this.efConst = efConst;
        return this;
    }

    /**
     * @param maxElements the maximal number of elements.
     * @return this builder.
     */
    public HnswBuilder withMaxElements(long maxElements) {
        if (maxElements < 0) throw new IllegalArgumentException("The ef constant must be positive.");
        this.maxElements = maxElements;
        return this;
    }

    /**
     * @param maxLayer the maximal number of layers.
     * @return this builder.
     */
    public HnswBuilder withMaxLayers(long maxLayer) {
        if (maxLayer < 0) throw new IllegalArgumentException("The ef constant must be positive.");
        this.maxLayer = maxLayer;
        return this;
    }

    /**
     * @return a hnsw instance for doing f32 calculations.
     */
    public Hnsw<F32> buildF32() {
        return build(F32.class);
    }

    /**
     * @return a hnsw instance for doing u16 calculations.
     */
    public Hnsw<U16> buildU16() {
        return build(U16.class);
    }

    /**
     * @param field over what field is the vector space?
     * @param <T>   field type.
     * @return an hnsw whose vectors are from a vector space of the given field.
     */
    public <T extends Field> Hnsw<T> build(Class<T> field) {
        Pointer metricIdPtr = PointerFactory.utf8(metric.id());
        // The rust interface that we target here expects usize which only admits positive values.
        // Hence, we make sure to only pass unsigned longs.
        Pointer ptr = hnswConstructor(field).of(
                maxNeighborhoodConnections,
                efConst,
                metric.id().length(),
                metricIdPtr,
                maxElements,
                maxLayer);
        Searcher srch = hnswSearch(field);
        Inserter insert = hnswInsert(field);
        Dropper dropper = hnswDrop(field);
        return new HnswImpl<>(ptr, metric, maxNeighborhoodConnections, efConst, maxElements, maxLayer, srch, insert, dropper);
    }

    private <T extends Field> Dropper hnswDrop(Class<T> field) {
        if (field == F32.class)
            return HnswRs.lib()::drop_hnsw_f32;
        if (field == U16.class)
            return HnswRs.lib()::drop_hnsw_u16;
        throw new IllegalStateException("Unexpected value: " + field);
    }

    private <T extends Field> HnswConstructor hnswConstructor(Class<T> field) {
        if (field == F32.class)
            return HnswRs.lib()::new_hnsw_f32;
        if (field == U16.class)
            return HnswRs.lib()::new_hnsw_u16;
        throw new IllegalStateException("Unexpected value: " + field);
    }

    private <T extends Field> Searcher hnswSearch(Class<T> field) {
        if (field == F32.class)
            return HnswRs.lib()::search_neighbours_f32;
        if (field == U16.class)
            return HnswRs.lib()::search_neighbours_u16;
        throw new IllegalStateException("Unexpected value: " + field);
    }

    private <T extends Field> Inserter hnswInsert(Class<T> field) {
        if (field == F32.class)
            return HnswRs.lib()::insert_f32;
        if (field == U16.class)
            return HnswRs.lib()::insert_u16;
        throw new IllegalStateException("Unexpected value: " + field);
    }

}
