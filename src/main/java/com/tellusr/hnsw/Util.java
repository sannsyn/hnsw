package com.tellusr.hnsw;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// A package private utility class.
final class Util {
    private Util() {
    }

    /**
     * @param x a long to be tested.
     * @throws IllegalArgumentException if the given long is negative.
     */
    static void requireNonNegative(long x) {
        if (x < 0) throw new IllegalArgumentException("Expected argument to be positive.");
    }

    /**
     * <b>Note:</b> this method is only intended for unit tests, not for production code.
     * <p>
     * A helper method to introduce the same functionality as java 9+s
     * {@code Map::of} method. These do not exist in java 8.
     * Unlike java 9s counterpart, this method is not optimized for a small number of key value pairs.
     *
     * @param args a list of key value pairs, in order. There must be an even number of arguments supplied.
     * @param <K>  the type of the key.
     * @param <V>  the type of the value.
     * @return the map determined by the input.
     */
    @SuppressWarnings({"unchecked", "DuplicatedCode"})
    // This code is duplicated in an example to avoid making it public.
    static <K,V> Map<K,V> mapOf(Object... args) {
        if (args.length == 0) return Collections.emptyMap();
        if (args.length % 2 != 0) throw new IllegalArgumentException();
        Map<K,V> m = new HashMap<>();
        for (int i = 0; i < args.length/2; i = i + 2) {
            m.put((K) args[i], (V) args[i+1]);
        }
        return Collections.unmodifiableMap(m);
    }
}
