package com.tellusr.hnsw;


/**
 * Models the f32 data type from rust. This class is used to get compile time generics checks for {@link Hnsw}.
 */
public final class F32 implements Field {
    private F32() {
    }
}
