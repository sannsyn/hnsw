package com.tellusr.hnsw;

import com.sun.jna.Pointer;

/**
 * An interface for initiating an instance of hnsw natively.
 * Can pass as either:
 * <ul>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#new_hnsw_f32(long, long, long, Pointer, long, long)}</li>
 *     <li>{@link com.tellusr.hnsw.internal.HnswRs#new_hnsw_u16(long, long, long, Pointer, long, long)}</li>
 * </ul>
 */
interface HnswConstructor {
    Pointer of(long max_nb_conn, long ef_const, long namelen, Pointer cdistname, long maxElements, long maxLayer);
}
