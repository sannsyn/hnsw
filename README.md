# HNSW (Hierarchical Navigable Small World) for jvm

This project consists of a java interface for [hnswlib-rs](https://github.com/jean-pierreBoth/hnswlib-rs). It is
currently only supported on Linux, and linking is handled by JNA.

The master branch contains tagged commits on the form v1.X. These are to be considered stable releases.

---

### Example use

```java
import com.tellusr.hnsw.Hnsw;
import com.tellusr.hnsw.Metric;
import com.tellusr.hnsw.U16;
import com.tellusr.hnsw.Vec;

import java.util.Map;

class Readme {
    public static void main(String[] args) {

        // Set up an instance of hnsw.
        Hnsw<U16> hnsw = Hnsw.builder()
                .withMaxNeighborhoodConnections(15)
                .withEfConst(200)
                .withMaxElements(1_000_000)
                .withMaxLayers(18)
                .withMetric(Metric.LEVENSHTEIN)
                .buildU16();

        // Build model by adding vectors, each with a unique ID.
        Map.of(
                0L, Vec.u16("abcd"),
                1L, Vec.u16("efgh"),
                2L, Vec.u16("ijkl"),
                3L, Vec.u16("abc"),
                4L, Vec.u16("test"),
                5L, Vec.u16("abbdc")).forEach(hnsw::insert);
        
        // Perform a search:
        var res = hnsw.search(Vec.u16("abcd"), 4, 30);
        
        System.out.println(res);

        // To deallocate native memory.  
        hnsw.drop();
    }
}
```

Output:

    [Neighbour{id=0, distance=0.0}, Neighbour{id=3, distance=1.0}, Neighbour{id=5, distance=2.0}, Neighbour{id=2, distance=4.0}]

---

To load a native library (.so file) to be used by JNA run:

```bash
./load_library.sh <path_to_library>
```

This will put the library in <i>src/main/resources/linux-x86-64</i>. JNA searches this directory.